#!/bin/sh
# shellcheck disable=SC2034

WORKDIR=$(dirname "${0}")

Ubuntu_pkg_list="curl"

if ! OPTS=$(getopt -o e:s:a:i:c:l:d:m: --long domain:,engine:,mode:,server:,agent:,ingress:,ingress-config:,loki-stack,loki-size:,volume-size: -n provisionner -- "${@}"); then
  echo "${OPTS}"
  exit 1
fi

eval set -- "$OPTS"

KUBE_ENGINE="k3d"
K3D_TAG="v5.4.3"       # k3d version
K3S_TAG="v1.24.4+k3s1" # k3s version
DEPLOY_MODE="dev"      # deploy modes are "dev" and "production"
SERVER=1
AGENT=3
TFMODE="nginx" # Use k3s traefik
DOMAIN="eole3.lan"
TFCNF=""
LOKI_STACK=0  # Install a loki-stack
LOKI_SIZE=1Gi # Persistent size for loki
PV_SIZE=2Gi   # Persistent volume size to offer
while true; do
  case "$1" in
    -m | --mode)
      DEPLOY_MODE="${2}"
      shift 2
      ;;
    -d | --domain)
      DOMAIN="${2}"
      shift 2
      ;;
    -e | --engine)
      KUBE_ENGINE="${2}"
      shift 2
      ;;
    -s | --server)
      SERVER="${2}"
      shift 2
      ;;
    -a | --agent)
      AGENT="${2}"
      shift 2
      ;;
    -i | --ingress)
      TFMODE="${2}" # install traefik from helm with value file (default or provided by the user)
      shift 2
      ;;
    -c | --ingress-config)
      TFCNF="${2}"
      shift 2
      ;;
    -l | --loki-stack)
      LOKI_STACK=1 # install loki-stack from helm
      shift
      ;;
    --loki-size)
      LOKI_SIZE="${2}Gi" # requested size for loki-stack
      shift 2
      ;;
    --volume-size)
      PV_SIZE="${2}Gi" # requested size for PV
      shift 2
      ;;
    --)
      shift
      break
      ;;
    *) break ;;
  esac
done

#
# @NAME: errMsg
# @AIM: Error message formater
# @PARAMS:
#   - The message
#   - The return code (0 for success)
# @RETURN: integer (0 for success)
#
errMsg() {
  message=${1}
  errCode=${2}
  echo
  echo "[ERROR] ${message}"
  if [ -z "${errCode}" ]; then
    errCode=1
  fi
  return "${errCode}"
}

#
# @NAME: installAptPkgs
# @AIM: Install packages with apt
# @PARAMS: a list of packages names
# @RETURN: integer (0 for success)
#
installAptPkgs() {
  timeout=3600
  printf "   - Repository update "
  if ! DEBIAN_FRONTEND=noninteractive flock --timeout 60 --exclusive --close /var/lib/dpkg/lock-frontend apt-get update </dev/null >/dev/null; then
    errMsg "Package repository update failed !" 3
    return ${?}
  fi
  printf '\n'

  for pkg in "${@}"; do
    echo "   - ${pkg}"
    if ! DEBIAN_FRONTEND=noninteractive apt-get install -y -qq "${pkg}" </dev/null >/dev/null; then
      errMsg "${pkg} install failed !" 2
      return ${?}
    fi
  done
  return 0
}

#
# @NAME: installPkgs
# @AIM: Wrapper for packages installation
# @PARAMS: a list of packages names
# @RETURN: integer (0 for success)
#
installPkgs() {
  case ${DISTRIB_ID} in
    Ubuntu | Debian)
      echo "${DISTRIB_DESCRIPTION}:"
      installAptPkgs "${@}"
      return $?
      ;;
    *)
      errMsg "Unsupported OS [${DISTRIB_ID}]" 10
      return ${?}
      ;;
  esac
  return 0
}

#
# @NAME: setupSSH
# @AIM: Update minimal configuration for SSH Daemon
# @PARAMS: None
# @RETURN: integer (0 for success)
#
setupSSH() {
  case ${DISTRIB_ID} in
    Ubuntu | Debian)
      echo "   - root login"
      sed -i -e 's/#PermitRootLogin.*/PermitRootLogin yes/' /etc/ssh/sshd_config
      echo "   - agent forwarding"

      sed -i -e 's/#AllowAgentForwarding.*/AllowAgentForwarding yes/' /etc/ssh/sshd_config
      echo "   - daemon restart"
      systemctl restart ssh
      return ${?}
      ;;
    *)
      errMsg "Unsupported OS [${DISTRIB_ID}]" 12
      return ${?}
      ;;
  esac
  return 0
}

#
# @NAME: install_bash_completions
# @AIM: Install bash completions for kubectl, helm, k3d....
# @PARAMS: None
# @RETURN: integer (0 for success)
#
install_bash_completions() {
  mkdir -p /etc/bash_completion.d/
  kubectl completion bash >/etc/bash_completion.d/kubectl
  helm completion bash >/etc/bash_completion.d/helm

  touch ~root/.screenrc
  if ! grep -q 'startup_message off' ~root/.screenrc; then
    cat >>~root/.screenrc <<EOF
# Don't display the copyright page
startup_message off
EOF
  fi

  if ! grep -q 'defshell -bash' ~root/.screenrc; then
    cat >>~root/.screenrc <<EOF

# To enable bash completion
defshell -bash
EOF
  fi

  if ! grep -q 'defscrollback ' ~root/.screenrc; then
    cat >>~root/.screenrc <<EOF

# keep scrollback n lines
defscrollback 1000
EOF
  fi
}

#
# @NAME: install_k3d
# @AIM: Install k3d+k3s software
# @PARAMS: None
# @RETURN: integer (0 for success)
#
installMkCert() {
  version="1.4.4"
  #FIXME architecture is hard coded
  file="mkcert-v${version}-linux-amd64"
  url="https://github.com/FiloSottile/mkcert/releases/download/v${version}/${file}"
  cd /tmp || return 3
  if ! wget -q ${url}; then
    errMsg "Error installing mkcert"
    return 3
  fi
  chmod +x ${file}
  mv ${file} /usr/bin/mkcert
  return ${?}
}

#
# @NAME: installTools
# @AIM: Install kubernetes tools (helm....)
# @PARAMS: None
# @RETURN: integer (0 for success)
#
installTools() {
  case ${DISTRIB_ID} in
    Ubuntu | Debian)
      # Install Docker
      installPkgs snapd apt-transport-https
      res=${?}
      if [ ${res} -ne 0 ]; then
        errMsg "Error installing docker.io or snapd" 3
        return ${res}
      fi

      installMkCert
      if ! installMkCert; then
        errMsg "Error installing mkcert" 3
        return 3
      fi

      # Install kubectl
      out=$(snap install kubectl --stable --classic 2>&1 >/dev/null)
      res=${?}
      if [ ${res} -ne 0 ]; then
        errMsg "Error installing kubectl"
        return ${res}
      fi

      # Install helm
      curl -s https://baltocdn.com/helm/signing.asc | APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add >/dev/null 2>&1
      echo "deb https://baltocdn.com/helm/stable/debian/ all main" >/etc/apt/sources.list.d/helm-stable-debian.list
      echo
      printf " Installing Helm for "
      installPkgs helm

      # Install bash completions
      install_bash_completions

      return 0
      ;;
    *)
      errMsg "Unsupported OS ${DISTRIB_ID}" 12
      return ${?}
      ;;
  esac
}

#
# @NAME: install_k3s
# @AIM: Install k3s+k3s software
# @PARAMS: None
# @RETURN: integer (0 for success)
#
install_k3s() {
  install_dir="/usr/bin"
  k3s_exec_opts=""
  # Install K3s
  install_script="https://get.k3s.io/"
  out=$(wget -O install.sh ${install_script} 2>&1 >/dev/null)
  res=${?}
  if [ ${res} -ne 0 ]; then
    errMsg "Error downloading k3d install script" 4
    return ${res}
  fi

  if [ ! "${TFMODE}" = "default" ]; then
    k3s_exec_opts="--disable=traefik"
  fi

  out=$(INSTALL_K3S_VERSION="${K3S_TAG}" INSTALL_K3S_BIN_DIR="${install_dir}" INSTALL_K3S_EXEC="${k3s_exec_opts}" sh install.sh 2>&1)
  res=${?}
  if [ ${res} -ne 0 ]; then
    errMsg "Error installing k3s"
    return ${res}
  fi

  systemctl enable --now k3s.service

  return 0
}

#
# @NAME: install_k3d
# @AIM: Install k3d+k3s software
# @PARAMS: None
# @RETURN: integer (0 for success)
#
install_k3d() {
  case ${DISTRIB_ID} in
    Ubuntu | Debian)
      # Install K3D
      installPkgs docker.io
      install_script="https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh"
      out=$(wget -O install.sh ${install_script} 2>&1 >/dev/null)
      res=${?}
      if [ ${res} -ne 0 ]; then
        errMsg "Error downloading k3d install script" 4
        return ${res}
      fi
      out=$(TAG="${K3D_TAG}" bash install.sh 2>&1)
      res=${?}
      if [ ${res} -ne 0 ]; then
        errMsg "Error installing k3d"
        return ${res}
      fi

      # Install bash completions
      k3d completion bash >/etc/bash_completion.d/k3d

      return 0
      ;;
    *)
      errMsg "Unsupported OS ${DISTRIB_ID}" 12
      return ${?}
      ;;
  esac
}

#
# @NAME: installEngine
# @AIM: Install kubernetes software
# @PARAMS: engine name
# @RETURN: integer (0 for success)
#
installEngine() {
  if ! installTools; then
    return "${?}"
  fi
  case "${1}" in
    k3d)
      install_k3d
      return "${?}"
      ;;
    k3s)
      install_k3s
      return "${?}"
      ;;
    *)
      errMsg "Unsupported kubernetes engine ${1}" 13
      return ${?}
      ;;
  esac
}

#
# @NAME: configure_k3d_daemon
# @AIM: Configure k3d .service
# @PARAMS: Cluster name (string)
# @RETURN: integer (0 for success)
#
configure_k3d_daemon() {
  name=${1}
  # Template unit
  echo "   - Create systemd service k3d@${name}.service"
  cat >/etc/systemd/system/k3d@.service <<EOF
[Unit]
After=docker.service
Wants=docker.service
ConditionPathExists=/usr/local/bin/k3d

[Service]
Type=oneshot
TimeoutSec=5min
RemainAfterExit=yes
ExecStart=/usr/local/bin/k3d cluster start %I
ExecStop=/usr/local/bin/k3d cluster stop %I

[Install]
WantedBy=multi-user.target
EOF

  ln -nfs /etc/systemd/system/k3d@.service "/etc/systemd/system/k3d@${name}.service"
  systemctl daemon-reload
  # Stop cluster before starting it with systemd
  out=$(k3d cluster stop "${name}" 2>&1)
  # Disable service if it is already enabled
  systemctl disable --now "k3d@${name}.service" >/dev/null 2>&1

  echo "   - Enable k3d@${name}.service"
  systemctl enable --now "k3d@${name}.service"
}

#
# @NAME: create_pv
# @AIM: Create a Persistent Volume
# @PARAMS: None
# @RETURN: integer (0 for success)
#
create_pv() {
  echo "   - Creating a Persistent Volume"

  cat >pv.yaml <<EOF
apiVersion: v1
kind: PersistentVolume
metadata:
  name: local-pv
  labels:
    type: local
spec:
  storageClassName: local-path
  capacity:
    storage: ${PV_SIZE}
  accessModes:
    - ReadWriteOnce
  local:
    path: "/storage"
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
            - k3d-eole3-server-0
            - k3d-eole3-agent-1
            - k3d-eole3-agent-0
            - k3d-eole3-agent-2
EOF

  kubectl apply -f pv.yaml >/dev/null 2>&1
}

#
# @NAME: install_traefik
# @AIM: install and configure traefik LB
# @PARAMS: None
# @RETURN: integer (0 for success)
#
install_traefik() {
  echo "   - Installing custom traefik"
  cnf=""
  if [ -z "${TFCNF}" ]; then
    cnf="./traefik.values.yaml"
  else
    cnf=${TFCNF}
  fi

  cat >"${cnf}" <<__EOF__
rbac:
  enabled: true
ports:
  web:
    redirectTo: websecure
  websecure:
    tls:
      enabled: true
podAnnotations:
  prometheus.io/port: "8082"
  prometheus.io/scrape: "true"
providers:
  kubernetesIngress:
    publishedService:
      enabled: true
  kubernetesCRD:
    enabled: true
    allowCrossNamespace: true
priorityClassName: "system-cluster-critical"
image:
  name: "rancher/mirrored-library-traefik"
tolerations:
  - key: "CriticalAddonsOnly"
    operator: "Exists"
  - key: "node-role.kubernetes.io/control-plane"
    operator: "Exists"
    effect: "NoSchedule"
  - key: "node-role.kubernetes.io/master"
    operator: "Exists"
    effect: "NoSchedule"
__EOF__

  helm repo add traefik https://helm.traefik.io/traefik >/dev/null 2>&1
  helm repo update >/dev/null 2>&1
  helm install traefik traefik/traefik -n kube-system -f "${cnf}" >/dev/null 2>&1
}

#
# @NAME: mkCert
# @AIM: Create local SSL certificate
# @PARAMS: domain name
# @RETURN: integer (0 for success)
mkCert() {
  domain="${1}"
  mkcert -install -key-file "/etc/ssl/private/${domain}.key" -cert-file "/etc/ssl/certs/${domain}.pem" "${domain}" "*.${domain}"
}

#
# @NAME: install_nginx
# @AIM: install and configure nginx ingress controler
# @PARAMS: None
# @RETURN: integer (0 for success)
#
install_nginx() {

  domain=${1}
  cnf="$(mktemp)"

  helm repo add bitnami https://charts.bitnami.com/bitnami
  helm repo update
  kubectl create namespace ingress

  # Create SSL certificate secret
  kubectl --namespace ingress create secret tls nginx-server-certs --key "/etc/ssl/private/${domain}.key" --cert "/etc/ssl/certs/${domain}.pem"

  cat >"${cnf}" <<__EOF__
extraArgs:
  default-ssl-certificate: "ingress/nginx-server-certs"
__EOF__

  helm install --namespace ingress -f "${cnf}" ingress bitnami/nginx-ingress-controller
}

#
# @NAME: install_loki_stack
# @AIM: install and configure loki-stack
# @PARAMS: None
# @RETURN: integer (0 for success)
#
install_loki_stack() {
  echo "   - Installing loki-stack"

  opts="grafana.enabled=true"
  opts="${opts},prometheus.enabled=true"
  opts="${opts},prometheus.alertmanager.persistentVolume.enabled=false"
  opts="${opts},prometheus.server.persistentVolume.enabled=false"
  opts="${opts},loki.persistence.enabled=true"
  opts="${opts},loki.persistence.storageClassName=local-path"
  opts="${opts},loki.persistence.size=${LOKI_SIZE}"

  helm repo add grafana https://grafana.github.io/helm-charts >/dev/null 2>&1
  helm repo update >/dev/null 2>&1
  helm install loki grafana/loki-stack --create-namespace -n loki-stack --set "${opts}" >/dev/null 2>&1
}

#
# @NAME: setup_k3s
# @AIM: Configure k3s
# @PARAMS: None
# @RETURN: integer (0 for success)
#
setup_k3s() {
  domain="${1}"
  echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >>"${HOME}/.profile"
  clusterProvisionning "${domain}"
}

#
# @NAME: setup_k3d
# @AIM: Configure k3d software
# @PARAMS: None
# @RETURN: integer (0 for success)
#
setup_k3d() {
  cname="${1}"
  domain="${2}"
  http_port="80"
  https_port="443"
  api_port="6443"
  servers="${SERVER}"
  agents="${AGENT}"
  storage_root="/srv/k3d/storage"
  storage_mnt_point="/storage"

  if [ ! -d "${storage_root}" ]; then
    mkdir -p "${storage_root}"
  fi

  opts="--servers ${servers} --agents ${agents} "
  opts="${opts} --port ${http_port}:80@loadbalancer"
  opts="${opts} --port ${https_port}:443@loadbalancer"
  opts="${opts} --volume ${storage_root}:${storage_mnt_point}"

  output=""
  if [ "${TFMODE}" = "default" ]; then
    # shellcheck disable=SC2086
    output=$(k3d cluster create "${cname}" ${opts} 2>&1)
  else
    # shellcheck disable=SC2086
    output=$(k3d cluster create "${cname}" ${opts} --k3s-arg "--disable=traefik@server:0" 2>&1)
  fi

  res=${?}
  if [ ${res} -eq 0 ]; then
    echo "   - Cluster ${cname} created"
  else
    echo
    echo "${output}"
    return ${res}
  fi

  clusterProvisionning "${domain}"

}

#
# @NAME: clusterProvisionning
# @AIM: Add some usefull tools to the "cluster"
# @PARAMS: None
# @RETURN: integer (0 for success)
#
clusterProvisionning() {
  if ! create_pv; then
    return ${?}
  fi

  if ! mkCert "${domain}"; then
    errMsg "SSL certificate creation failed (domain: ${domain})" 2
    return ${?}
  fi

  if [ "${TFMODE}" = "traefik" ]; then
    install_traefik
    res=${?}
    if [ ${res} -ne 0 ]; then
      return ${res}
    fi
  fi

  if [ "${TFMODE}" = "nginx" ]; then
    if ! install_nginx "${domain}"; then
      return ${?}
    fi
  fi

  if [ ${LOKI_STACK} -eq 1 ]; then
    install_loki_stack
    res=${?}
    if [ ${res} -ne 0 ]; then
      return ${res}
    fi
  fi

  return 0
}

#
# @NAME: setupEngine
# @AIM: Configure kubernetes software
# @PARAMS: engine name
# @RETURN: integer (0 for success)
setupEngine() {
  cname="eole3"
  case ${1} in
    k3d)
      if ! setup_k3d "${cname}" "${DOMAIN}"; then
        return "${?}"
      fi
      configure_k3d_daemon "${cname}"
      return "${?}"
      ;;
    k3s)
      setup_k3s "${DOMAIN}"
      return "${?}"
      ;;
    *)
      errMsg "Unsupported kubernetes engine ${1}" 13
      return ${?}
      ;;
  esac
}

if lsb_release >/dev/null 2>&1; then
  DISTRIB_ID=$(lsb_release --short --id)
  DISTRIB_RELEASE=$(lsb_release --short --release)
  DISTRIB_DESCRIPTION=$(lsb_release --short --description)
else
  errMsg "Command lsb_release is missing : Unsupported OS"
  exit 2
fi

case "${DEPLOY_MODE}" in
  dev | production)
    echo "Deployement mode : ${DEPLOY_MODE}"
    ;;
  *)
    errMsg "Deploy mode \"${DEPLOY_MODE}\" is not \"dev\" or \"production\"" 3
    exit "${?}"
    ;;
esac

echo "*********************************************************"
echo "*                 Eolebase3 Provisionner                *"
echo "*********************************************************"

if [ "${DEPLOY_MODE}" = "dev" ]; then
  # SSH Setup
  echo " SSH daemon setup (Dev mode only)"
  setupSSH
  echo
fi

# Installing packages
printf " Package installation for "
pkg_list=$(eval "echo \${${DISTRIB_ID}_pkg_list}")
# shellcheck disable=SC2086
if ! installPkgs ${pkg_list}; then
  errMsg "Packages installation failed for ${DISTRIB_ID}" 4
  exit ${?}
fi

echo

if [ "${DEPLOY_MODE}" = "production" ]; then
  KUBE_ENGINE="k3s"
fi

# Installing Kubernetes
printf " Installing kubernetes engine %s" "${KUBE_ENGINE} "
if ! installEngine "${KUBE_ENGINE}"; then
  errMsg "${KUBE_ENGINE} installation failed !" 5
  exit "${?}"
fi

# Setup engine
echo
echo " Setup ${KUBE_ENGINE}"

if ! setupEngine "${KUBE_ENGINE}"; then
  errMsg "Error during engine ${KUBE_ENGINE} setup"
  echo
  echo "*********************************************************"
  exit 1
fi
echo
echo "*********************************************************"
